#TELEGRAM BOT LIBRARY + SENSOR PROJECT

## CONFIGURATION

### HOST_IP
 - change `host_ip` variable to your own `public ip`

### Need certificate
 - Certificate for webhook api
 - Webhook api is `ssl required`
 - Need to sign with `HOST IP`
 - Save with `PUBLIC.key`, `PRIVATE.pem` into `./CERT` directory
 - Checkout `https://core.telegram.org/bots/self-signed`
 
## start command
 1. npm install
 1. node index.js
 1. end

### HOW IT WORKS
 - It watching `sendsor.json` file
 - The thing you need is just write sensor output on `sensor.json` file
 - And It will work with telegram
 
 