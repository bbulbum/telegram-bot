var request = require("request");
var fs = require("fs");
var base_url = "https://api.telegram.org";

module.exports = function(token) {
  var api_url = base_url + "/bot" + token;
  return {
    getUpdates: function() {
      request.get(api_url+"/getUpdates", function(err, res, body) {
        return body;
      });
    },

    sendMessage: function(chat_id, message) {
      var data = {form: { chat_id: chat_id, text: message }};
      request.post(api_url+"/sendMessage", data, function(err, res, body){
        return body;
      });
    },

    sendPhoto: function(chat_id, picName) {
      var file = fs.createReadStream('./pictures/'+picName);
      var data = {formData: { chat_id: chat_id, photo: file }};
      request.post(api_url+"/sendPhoto", data, function(err, res, body){
        console.log(body);
        return body;
      });
    },

    getFile: function(file_id, callback) {
      var data = {form: { file_id: file_id }};
      request.post(api_url+"/getFile", data, function(err, res, body){
        var file_path = JSON.parse(body).result.file_path;
        return callback(base_url+"/file/bot"+token+"/"+file_path);
      });
    },

    deleteWebhook: function() {
      request.get(api_url+"/deleteWebhook", function(err, res, body){
      });
    },

    setWebhook: function(host_url, certFile) {
      var data = {
        formData: {
          url: host_url+"/bot_"+token,
          certificate: {
            value:  fs.createReadStream(certFile),
            options: {
              filename: 'PUBLIC.pem',
              contentType: 'application/x-pem-file',
            },
          },
        },
      };

      request.post(api_url+"/setWebhook", data, function(err, res, body){
        console.log(body);
        request.get(api_url+"/getWebhookInfo", function(err, res, body) {
          console.log(body);
        });
      });
    }
  }
}
