var request = require("request");
var fs = require("fs");
var messages = require("./messages.json");
var https = require("https");
var urlParser = require("url");

var token = "343444354:AAEjyoIsTQYvL9bMTCBJ3YYFICkN7hkZSY4";
var telegram = require("./telegram")(token);
var host_url = "https://13.124.118.27";
var sensorFile = "./sensor.json";

fs.watchFile(sensorFile, function(curr, prev) {
  try {
    var sensor = fs.readFileSync(sensorFile, "utf8").trim();
    sensor = JSON.parse(sensor);
    var event = sensor.event;
    var picture = sensor.fileName;

    sendMessage(messages[event]);
    if (event == "opened") {
      sendPhoto(picture);
    }
  } catch (e) {
    console.log(e);
  }
});

var options = {
  key: fs.readFileSync('./CERT/PRIVATE.key'),
  cert: fs.readFileSync('./CERT/PUBLIC.pem'),
};

var msg = {
  "안녕" : [
    "그래 안녕",
    "안녕 못한다",
    "ㅇㅇ",
    "(무시)",
  ],
  "야" : [
    "왜불러",
    "왜!",
    "엉 무슨일이야~",
  ],
  "최규범" : [
    "천재",
    "멋쟁이",
  ],
}

var sendReply = function(message) {
  var chat_id = message.chat.id;
  var text = message.text;
  switch (text) {
    case "사진":
      telegram.sendPhoto(chat_id, "pic01.png");
      break;
    default:
      var msgs = msg[text];
      var random = msgs ? parseInt(Math.random() * 10 % msgs.length, 10) : null;
      var reply = random != null ? msgs[random] : "무슨말이야?";
      telegram.sendMessage(chat_id, reply);
      break;
  }
}

https.createServer(options, function(req, res) {
  var url = urlParser.parse(req.url);

  if (url.pathname == "/bot_"+token ) {
    var body = [];
    req.on('data', function(chunk) {
      body.push(chunk);
    }).on('end', function() {
      body = Buffer.concat(body).toString();
      try {
        var bodyObj = JSON.parse(body);
        var message = bodyObj.message;
        if (message.photo) {
          message.photo.map(function(photo){
            telegram.getFile(photo.file_id, function(fileUrl) {
              console.log(fileUrl);
            });
          })
        }
        if (message.text) {
          sendReply(bodyObj.message);
        }
      } catch (e) {
        console.log(e);
      }
    });
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end("hi");
    return;
  }

  res.writeHead(404, {'Content-Type': 'text/plain'});
  res.end("404");
}).listen(443);

telegram.deleteWebhook();

setTimeout(function(){
  telegram.setWebhook(host_url, './CERT/PUBLIC.pem');
}, 1500);
